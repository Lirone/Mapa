#include "stdafx.h"
#include "Traits.h"
#include <iostream>
using namespace std;
void Traits::Display()
{
	cout << letter;
}
Traits::Traits()
{
	//cout << "Konstruktor bez wartosci" << endl;
	this->x = 0;
	this->y = 0;
}

Traits::Traits(int x, int y)
{
	//cout << "Konstruktor z warosciami" << endl;
	this->x = x;
	this->y = y;
}
Traits::Traits(const Traits & wektor)
{
	//cout << "Konstruktor kopiujacy" << endl;
	x = wektor.x;
	y = wektor.y;
}
Traits Traits::operator+(const Traits & v1) const
{
	return Traits(this->x + v1.x, this->y + v1.y);
}
Traits Traits::operator-(const Traits & v)
{
	return Traits(this->x - v.x, this->y - v.y);
}
Traits Traits::operator/(const Traits & v)
{
	return Traits(this->x / v.x, this->y / v.y);
}

Traits Traits::operator=(const Traits & v) // by� �le przeci��ono operator =
{
	x = v.x;
	y = v.y;
	return *this;
}

Traits Traits::operator*(const int &d)
{
	Traits wektor_multiply;
	wektor_multiply.x = this->x * d;
	wektor_multiply.y = this->y * d;
	return wektor_multiply;
}

Traits Traits::operator<<(const Traits & v)
{
	return Traits();
}

Traits::~Traits()
{
}
