#pragma once
#include <ostream>
#include <iostream>
#include <vector>

using namespace std;
class Traits
{
public:
	int x;
	int y;
	char letter =NULL;
	virtual void Display();
	Traits();
	Traits(int x, int y);
	Traits(const Traits &wektor);
	Traits operator +(const Traits &v)const;
	Traits operator-(const Traits & v);
	Traits operator/ (const Traits & v);
	Traits operator=(const Traits &v);
	Traits operator *(const int &d);
	Traits operator <<(const Traits & v);
	~Traits();
};

